<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'mystory-theme');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '14789632');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CPnd0A13GNWT[78v5`w[|H>GfskS:+RVVcDA;lI-u (wLV!oVBI&?}+5$5LOzn74');
define('SECURE_AUTH_KEY',  'A!PHKd=#r6)rTk#,/o3)xpx-Lai5TX3gYgkp0l>*fb))E[{ziDY-a#WYEfk]a12E');
define('LOGGED_IN_KEY',    ';j4hhui]q#UPZRAkvbEM57K^_,u[YU]gHz*vgx)RwH:B_E_kZY7vUw#1L9E6at1w');
define('NONCE_KEY',        '=q=0@3o=Iic>&f&Q<6ekk#6f +C1xCGXc;+<$]ZFenIZ{!+TSSwsU>>,Ai$0ex:L');
define('AUTH_SALT',        '0mG74d1bc6~SG#[8Fu5P:0)|{f]G&[gD~NI/]ZYd(d5=11kusng,3PGtqL^~CH!R');
define('SECURE_AUTH_SALT', 'B)4u$J(R{,ow/W)(mB}aw0T*~P#bXK (vA&mC`r>$Z8J/Pj*+a2hCPVROd62OX1W');
define('LOGGED_IN_SALT',   ':Z@OCU4Xn:tv?R{x4q R9PIE0dG;EXONcW/^t>sIGE1Iin]8+;N|6)RBP=KlhjoL');
define('NONCE_SALT',       '~sg;3ayl`29>`BqNW%IX-(/T7X}RFBH HeJ+pT38oqsBVlY,W3 g4_>uO{!{3%9/');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'ms_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
